function [density]=subgraphDensity(A, node_set)
%% SUBGRAPHDENSITY Returns the density for the given node_set in
%% graph with adjacency matrix A (assumed symmetric); 
% 
%  Usage: [density] = subgraphDensity(A, node_set)
%  
    if nargin < 2
        node_set  = [1:size(A, 1)]; 
    end
    
    As = A(node_set, node_set); 
    num_edges = sum(sum(As, 1), 2); 
    num_nodes = size(As, 1); 
    density = num_edges/(num_nodes *(num_nodes - 1)); 
end

