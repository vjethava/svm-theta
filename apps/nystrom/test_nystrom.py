import networkx as nx
import numpy as np
from sklearn.kernel_approximation import Nystroem as Nystroem

n = 1000
p = 0.5
G = nx.random_graphs.erdos_renyi_graph(n, p)
A = nx.to_numpy_matrix(G)
eig_vals = np.linalg.eigvalsh(A)
rho = np.min(eig_vals)
K = np.eye(n) + A / rho
Kfn = lambda x, y: K[x, y]
kernel_approximator = Nystroem(kernel=Kfn, gamma=None, coef0=1, degree=3, kernel_params=None, n_components=300)
X = np.arange(n) 
kernel_approximator.fit(X)
