function [metadata] = load_rxn_metadata(file_name)
% LOAD_RXN_METADATA loads and returns the reaction metadata in a tabular
% format
%
% Usage: [metadata] = load_rxn_metadata(file_name) 
% 
if nargin < 1
    file_name = 'rxn_metadata.txt'; 
end

fprintf(2, 'load_rxn_metadata(): reading metadata file: %s\n', file_name); 
% this part is tied to rxn_metadata specification provided
format_spec = ['%s%s' repmat('%d', 1, 84)]; 
header_spec = repmat('%s',1, 85); 

% read header line and data
fid = fopen(file_name, 'r'); 
header_list = textscan(fid, header_spec, 1, 'Delimiter', '\t', 'CollectOutput', 0); 
metadata = textscan(fid, sprintf('%s',format_spec), 'Delimiter', '\t', 'CollectOutput', 0, 'HeaderLines', 1); 
fprintf(2, 'load_rxn_metadata(): read %d rows\n', length(metadata{1})); 
fclose(fid); 

% process the header line
for i=1:length(header_list)
     header_list{i} = sprintf('%s' , header_list{i}{1}); 
end
header_list = strrep(header_list, ':', '_');
header_list = strrep(header_list, '.', '_'); 
header_list = strrep(header_list, '(', '_'); 
header_list = strrep(header_list, ')', '_'); 

% remove beginning and trailing _ 
header_list = regexprep(header_list, '^_+', ''); 
header_list = regexprep(header_list, '_+$', ''); 

% return the table
metadata = cell2table(metadata(2:86), 'VariableNames', header_list);
