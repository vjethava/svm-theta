function [alpha, model]= findCommonDenseSubgraphCSVM(G, graph_weights)
% Function to find common dense subgraph using weighted adjacency matrix for common set of graphs
% [S]= findCommonDenseSubgraphCSVM(G)
m = length(G); 
n = G{1}.N; 
if nargin < 2 
    graph_weights = ones(m, 1); % all graphs equally important
end
A = zeros(n, n); 
for i=1:m 
    A =  A + graph_weights(i) .* double(G{i}); % weighted adjacency matrix
end
A = A ./ sum(graph_weights); % adjacency adjusted to between [0, 1]
                        
A = (A + A')/2.0; % make graph undirected and symmetric


% Al = tril(A, 1); 
% Ah = triu(A, 1
%if ~all(Ah == Al') % matrix is directed 
%    A = (Al + Al' + Ah + Ah') / 2;
% end

    
A_c = ones(n, n) - eye(n) - A; % graph complement
fprintf(2, 'findCommonDenseSubgraphCSVM(): Computed weighted adjacency matrix. Computing labelling ...\n'); 
K_c = getPsdKfromA(A_c, 'use_eigen', 1); % compute labelling 
fprintf(2, 'findCommonDenseSubgraphCSVM(): Computed labelling. Starting SVM ... \n'); 

tic; 
[model] = findIndependentSetCSVM(K_c);   % run SVM 
tc = toc; 
alpha = model.x ; 
fprintf(2, 'findCommonDenseSubgraphCSVM(): Finished SVM in time: %g\n', tc); 



