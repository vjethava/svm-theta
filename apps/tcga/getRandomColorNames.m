function [color_names] = getRandomColorNames(m) 
% GETRANDOMCOLORNAMES returns names of m X11 colors read from file x11_colors.txt
%
% Usage: [color_names] = getRandomColorNames(m) 
T = readtable('x11_colors.txt', 'ReadVariableNames', false); 
x11_color_names = T.Var1; 
nc = length(x11_color_names); 
co = randperm(nc); 
for i=1:m
    color_names{i} = x11_color_names{co(i)}; 
end
