function [H] = do_node_intersection(G)
num_graphs = length(G); 
H = cell(num_graphs, 1); 
common_node_mp = G{1}.node_names; 
old_idx = cell(1, num_graphs); 
new_idx = cell(1, num_graphs); 
% add the keys to obtain a common node set - this can be made faster. 
for i=2:num_graphs
    curr_graph = G{i}; 
    curr_node_mp = curr_graph.node_names; 
    curr_num_nodes = length(curr_node_mp);
    c_old_idx = zeros(curr_num_nodes, 1); 
    c_new_idx = zeros(curr_num_nodes, 1); 
    x = 0; 
    for k=curr_node_mp.keys()
        x = x + 1; 
        if ~common_node_mp.isKey(k)
            common_node_mp.remove(k);
        else
            c_old_idx(x) = curr_node_mp(k);            
            c_new_idx(x) = common_node_mp(k);
        end
    end
    old_idx{i} = c_old_idx(c_old_idx ~= 0); 
    new_idx{i} = c_new_idx(c_new_idx ~= 0); 
end

for i=1:num_graphs
    curr_graph = G{i}; 
    old_data = double(curr_graph); 
    new_data = sparse(total_node_num, total_node_num);        
    new_data(new_idx{i}, new_idx{i}) = old_data(old_idx{i}, old_idx{i}); 
    curr_name = curr_graph.name;
    H{i} = Graph.RxnRxnGraph(new_data, common_node_mp, curr_name); 
end
    
