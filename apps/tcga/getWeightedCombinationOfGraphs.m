function [A] = getWeightedCombinationOfGraphs(G, graph_weights, symmetrize)
% Compute weighted adjacency matrix by combining different graphs according to 
% corresponding importance specified by weights 
%
% Usage: [A] = getWeightedCombinationOfGraphs(G, graph_weights, symmetrize)
% 
% Expects: 
% --------
% G: collection of graphs (m x 1 cell) 
% graph_weights: mx1 weight vector for weighting (default: ones(m, 1))
% symmetrize: if =1, symmetrize the adjacency matrix (A = 9A+A')/2 )
%
% Returns: 
% --------
% A - weighted adjacency matrix 
% 


m = length(G); 
n = G{1}.N; 
if nargin < 2 
    graph_weights = ones(m, 1); % all graphs equally important
end
if nargin < 3
    symmetrize = 1;  % compute symmetric adjacency matrix 
end


A = zeros(n, n); 
for i=1:m 
    A =  A + graph_weights(i) .* double(G{i}); % weighted adjacency matrix
end
A = A ./ sum(graph_weights); % adjacency adjusted to between [0, 1]

if symmetrize
    A = (A + A')/2.0; % make graph undirected and symmetric
end