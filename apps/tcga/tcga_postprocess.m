function [subgraph_node_names, G1, subgraph_densities, subgraph_idx] = tcga_postprocess(G, alpha, min_subgraph_size, max_subgraph_size)
% Extract subgraph-based on support vectors (alpha) and return updated graph 
%  
% Usage: [subgraph_node_names, G1, subgraph_densities, subgraph_idx] = tcga_postprocess(G, node_names, alpha, min_subgraph_size, max_subgraph_size)
% 
% 
DEBUG = 0; 

num_graphs = length(G); 
n=  G{1}.N; 
node_names = G{1}.node_names; 
node_order = []; 
subgraph_densities = zeros(num_graphs, 1); 

if nargin < 5
   max_subgraph_size = n; 
end


fprintf(2, 'tcga_postprocess(): finding node order\n'); 
ordered_subgraph_density = zeros(n, num_graphs); 
for i=1:num_graphs
    [node_order, curr_ordered_sd , alpha_sorted] = Graph.get_ordered_density(G{i}, alpha, -1); 
    ordered_subgraph_density(:, i) = curr_ordered_sd;  
end


% heuristic selection of subgraph size based on minimax 
ordered_min_density = min(ordered_subgraph_density, [], 2); 

[dense_subgraph_density, dense_subgraph_size] = max(ordered_min_density(min_subgraph_size:min(n, max_subgraph_size)  )); 
dense_subgraph_size = dense_subgraph_size + min_subgraph_size - 1; 

% heuristic pruning - in case MKL did not finish due to memory issues
deg_node = zeros(num_graphs, dense_subgraph_size); 
subgraph_idx_1 = node_order(1:dense_subgraph_size); 

S = {}; 
overall_isolated = [1:dense_subgraph_size];  
for i=1:num_graphs
    A = double(G{i}); 
    S{i} = full(A(subgraph_idx_1, subgraph_idx_1));    
    if DEBUG 
        d_incoming = sum(S{i}, 1); 
        d_outgoing = sum(S{i}, 2); 
        for k=1:length(subgraph_idx_1) 
            curr_node = node_names{subgraph_idx_1(k)}; 
            fprintf(2, 'Node: %s has degree %d and %d in graph %d\n', ...
                    curr_node, d_incoming(k), d_outgoing(k), i); 
        end
    end
    no_incoming = find(sum(S{i}, 2) == 0); 
    no_outgoing = find(sum(S{i}, 1) == 0); 
    curr_isolated = intersect(no_incoming, no_outgoing) ;
    overall_isolated = intersect(curr_isolated, overall_isolated); 

end



connected_idx = setdiff([1:dense_subgraph_size], overall_isolated);  
fprintf(2, 'tcga_postprocess():  connected: %d total: %d\n', length(connected_idx), dense_subgraph_size); 
subgraph_idx = node_order(connected_idx); 
subgraph_node_names = node_names(subgraph_idx); 

for i=1:num_graphs
    A = double(G{i}); 
    subgraph_densities(i) = subgraphDensity(A, subgraph_idx); 
end


G1 = {}; 
for i=1:num_graphs
    A = double(G{i}); 
    A(subgraph_idx, subgraph_idx) = 0; 
    G1{i} = Graph.RxnRxnGraph(A, G{i}.node_names, G{i}.name ); 
    assert(norm(A - double(G1{i}), Inf) < 1e-6, 'Error in graph conversion'); 
end

