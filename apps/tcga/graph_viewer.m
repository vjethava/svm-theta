function [file_name] = graph_viewer(G, node_order, graph_colors, file_name, options) 
m = length(G); 
node_names = G{1}.node_names; 
si = node_order; 
fig_handle = {}; 
if nargin < 3
   graph_colors = getRandomColorNames(m); 
end

if nargin < 4
    file_name = tempname ; 
end

if nargin < 5
    options.graphviz_visualize = 0; 
    options.matrix_visualize = 1; 
end

[pathstr, name, ext] = fileparts(file_name); 
if isempty(pathstr)
    pathstr = pwd;
end

if options.graphviz_visualize
    gv_file = [file_name '.gv']; 
    
    img_format = 'png'; 
    img_file = [pathstr '/' name '.' img_format] ; 
    write_dot_file(G, graph_colors, node_names(si), si,  gv_file); 
    unix(sprintf('neato -Kdot -Tpng %s > %s', gv_file, img_file), '-echo'); 
    unix(sprintf('open %s', img_file), '-echo'); 
end

if options.matrix_visualize
    fname = [pathstr '/' name '_spy']; 
    do_spy_plot(G, node_names, si, fname); 
end

% %% Graphviz4matlab code - failing for large graphs
% for i=1:m
%     A = double(G{i}); 
%     S = A(si, si); 
%     fig_handle{i} = graphViz4Matlab('-adjMat', S, '-nodeLabels', node_names(si), '-nodeColors', graph_colors{i} );
%     % fig_handle{i}.growNodes;     
%     % fig_handle{i}.growNodes;     
%     % fig_handle{i}.tightenAxes; 
% end

function do_spy_plot(G, node_names, node_ids, fname) 
m = length(G); 
ns = length(node_names); 
ni = length(node_ids);  
h = figure; 
colors = {'r'; 'g'; 'b'; 'c'; 'm' ; 'y'; 'k'}; 
if ni < 50
    markersize = 15; 
    yticklabel_fontsize = 8; 
elseif ni < 100
    markersize = 10; 
    yticklabel_fontsize = 5; 
else 
    markersize = 2; 
    yticklabel_fontsize = 1;
end


if m <= 2
    nrows = m; 
    ncols = 1; 
else
    nrows = ceil(sqrt(m)); 
    ncols = nrows; 
end

set(gcf, 'PaperType', 'a4');
% set(gcf, 'PaperOrientation', 'landscape');
csd = zeros(m, 1); 
for i=1:m
    A = double(G{i}); 
    S = full(A(node_ids, node_ids));
    col_idx = mod(i-1, length(colors)) + 1;
    csd(i) = subgraphDensity(S);
    %  plot the subplot
    subplot(nrows, ncols, i); 
    spy(S, sprintf('.%s', colors{col_idx}), markersize); 
    title(sprintf('d: %.4g', csd(i))); 
    xlabel(''); 
    % set(gca, 'XTickLabel', {}); 
    % set(gca, 'YTickLabel', {}); 
end

% not too many nodes and not too many graphs
if (ni < 100) && (m < 20)
    subplot(nrows, ncols, nrows*ncols); 
    axis([0 (ni+1) 0 (ni+1)]); 
    title(sprintf('|S|: %d', ni)); 

    a = get(gca,'YTickLabel');
    % only show labels for small subgraphs
    set(gca,'YTickLabel',a,'FontName','Times','fontsize',yticklabel_fontsize);
    set(gca, 'YTick', [1:ni]); 
    set(gca, 'YTickLabel', node_names(node_ids));
end
savefig(fname); 
print('-dpng', '-r600', fname); 
close(h); 

function []=write_dot_file(G, graph_colors, node_names, node_ids, file_name)
m = length(G); 
ns = length(node_names); 
fid = fopen(file_name, 'w');
fprintf(fid, 'digraph G {\n'); 
fprintf(fid, '\tedge [penwidth=2];\n'); 
fprintf(fid, '\tnode [shape=circle];\n'); 
for i=1:ns % write out the nodes
    fprintf(fid, '\t%s;\n', node_names{i});
end
for i=1:m
    A = double(G{i}); 
    S = A(node_ids, node_ids); 
    [vi, vj, vv] = find(S); 
    curr_color = graph_colors{i}; 
    ne = length(vi); 
    for j=1:ne
        ci = vi(j); 
        cj = vj(j); 
        fprintf(fid, '\t%s -> %s [color=%s];\n', node_names{ci}, node_names{cj}, curr_color);
    end
end
fprintf(fid, '}');
fclose(fid); 