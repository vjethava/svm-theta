% This script preprocesses TCGA subnetworks for faster loading 
% 
% See Also:
% ---------
% tcga_subnetworks.m 
% 
function preprocess_graphs() 
warning off; 
% paths - (NOTE: Specify full path)
data_dir = '/Users/vjethava/polybox/Code/Francesco/' ; 
svmtheta_dir = '/Users/vjethava/Code/svm-theta'; 
metadata_file = [data_dir '/' 'rxnRxnNetwork_metadata.txt']; 

mkdir([data_dir '/mat']); 

% setup SVM path
if isempty(getenv('SVM_THETA_HOME'))
    run([svmtheta_dir '/setup.m']); 
end

% load rxn_metadata
rxn_metadata = load_rxn_metadata(metadata_file); 

% The TCGA rxn-rxn network files
file_names = rxn_metadata.models_rrnetw_name{1:end}; 
file_names = cellfun(@(x) [data_dir '/' x], file_names, 'UniformOutput', false); 

num_graphs = length(file_names);

parfor i=1:num_graphs
    fprintf(2, 'read_graphs(): %d/%d dense subgraph\n', i, num_graphs); 
    curr_file = file_names{i};     
    G_dense = Graph.readRxnRxnGraph(curr_file); 
    G_sparse = G_dense.complement; 
    [pathstr, name, ext] = fileparts(curr_file);    
    new_file = [pathstr '/mat/' name '.mat'];
    fprintf(2, 'preprocess_graph(): Wrote to file: %s\n', new_file); 
    parsave(new_file, G_dense, G_sparse) ; 
end

% save preprocessed_rxn_rxn_graphs.mat G_dense G_sparse; 

function parsave(fname, G_dense, G_sparse)
% Saving within parfor loop 
save(fname, 'G_dense', 'G_sparse'); 

