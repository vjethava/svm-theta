% This files extracts common dense subgraphs based on TCGA networks
% 
% See [Francesco data]
% clear all; clc; close all; 

warning off; 
% paths - (NOTE: Specify full path)
data_dir = '/Users/vjethava/polybox/Code/Francesco' ; 
mat_data_dir = [data_dir '/mat/']; 
svmtheta_dir = '/Users/vjethava/Code/svm-theta'; 
metadata_file = [data_dir '/' 'rxnRxnNetwork_metadata.txt']; 
% setup SVM path
if isempty(getenv('SVM_THETA_HOME'))
    run([svmtheta_dir '/setup.m']); 
end

figure_dir = [SVM_THETA_HOME '/apps/tcga/figures/']; 

output_file = 'tcga_output.mat'; 

delete(output_file); % remove previous runs 
save(output_file, 'data_dir', 'mat_data_dir', 'metadata_file'); 

% load rxn_metadata
rxn_metadata = load_rxn_metadata(metadata_file); 

% The TCGA rxn-rxn network files
file_names = rxn_metadata.models_rrnetw_name{1:end}; 
file_names = cellfun(@(x) [data_dir '/' x], file_names, 'UniformOutput', false); 

% number and maximum size of cliques to extract 
min_subgraph_size = 5; 
max_subgraph_size = 100; % leave it to heuristic choice 
num_cliques = 3;

save(output_file, 'min_subgraph_size', 'max_subgraph_size', 'num_cliques', '-append'); 

% script options 
RUN_MKL_FLAG = 0; % run MKL-based analysis `
RUN_SVM_FLAG = 1; % run SVM-based analysis 
RUN_DENSE_APPROX_FLAG=1; 
VERBOSE_MODE = 0; 

viewer_options = struct(); 
viewer_options.graphviz_visualize = 0; 
viewer_options.matrix_visualize = 0; 

%% Choosing to find dense subgraphs in some graphs
% %% any combination of dense and sparse subgraphs is feasible by selecting
% %% the right graph indices - for e.g. 
% %% 
% %%   dense_ids = [1, 4, 5]; 
% %%   sparse_ids = [2, 10, 7]; 
% %% 
% %% will attempt to analyze graphs (1, 4, 5, 2, 10, 7) and try to find
% %% subgraphs that are dense in graphs (1, 4, 5) and sparse in the others

attribute = 'metadatabrca'; 
dense_ids = find(cell2mat(rxn_metadata.(attribute)));
sparse_ids = []; 
fprintf(2, 'tcga_subnetworks(): Using attribute %s for finding common dense subgraphs\n', attribute ); 

%% Setting dummy set of ids for testing 
dense_ids = [1; 2; 5]; % dummy set of ids for testing;

fprintf(2, 'tcga_subnetworks(): Set dummy dense_id: %s\n', num2str(dense_ids));  


save(output_file, 'dense_ids', 'sparse_ids', '-append'); 

%% load the selected graphs in Graph.RxnRxnGraph format
selected_graphs = read_graphs(file_names, dense_ids, sparse_ids); 
[G, common_node_list] = do_node_union(selected_graphs); 
num_graphs = length(G); 

%% some bookkeeping and visualization options  
graph_colors = getRandomColorNames(num_graphs); 

%% Use Greedy approximation for finding densest subgraph 
if RUN_DENSE_APPROX_FLAG 
    % construct common dense wrighted graph 
    A_combined = getWeightedCombinationOfGraphs(G); 
    [best_S, best_density, dsa_res] = densest_subgraph_approx(A_combined, VERBOSE_MODE); 
    
    nodes = common_node_list(best_S); 
    charikar_result = struct('id', best_S, 'name', nodes); 
    
    fprintf(2, '\nSubgraph having %d nodes using [Charikar ''00] method\n', length(nodes)); 
    disp(nodes); 
    
    save(output_file, 'charikar_result', '-append'); 
    
    charikar_fig = [figure_dir filesep 'charikar_dense']; 
    viewer_options.matrix_visualize = 1; 
    graph_viewer(G, best_S, graph_colors, charikar_fig, viewer_options);     
    % choose subgraph with best RELATIVE DENSITY 
    % if S is subgraph  
    %   |S| = number of nodes in  S
    %   |E_S| = number of edges in S 
    % Then, 
    %   density: |E_S| / |S|
    %   relative_density: = |E_S| / (|S| * (|S|-1)) = density(S)/(|S| - 1) 

end

G_orig = {}; 
for i=1:num_graphs
    G_orig{i} = G{i}; 
end

    
%% Use SVM with weighted adjacency matrix
if RUN_SVM_FLAG
    fprintf(2, 'tcga_subnetworks(): Running SVM-based analysis (does averaging of graphs)\n'); 
    extracted_subgraphs = struct('node_names', {}, 'densities', {}, 'node_ids' , {}); 
    overall_set_of_extracted_node_ids = []; 
    
    %%  start processing using SVM 
    for i = 1:num_cliques
        [alpha, model] = findCommonDenseSubgraphCSVM(G); 
        [subgraph_node_names, G1, subgraph_densities, subgraph_node_ids]=  tcga_postprocess(G, ...
                                                           alpha, min_subgraph_size, max_subgraph_size); 
        k = length(subgraph_node_ids); 
        fprintf(2, 'tcga_subnetwork(): Extracted subgraph of size %d with min_density: %g\n',k, min(subgraph_densities)); 
        disp(subgraph_node_names'); 
        extracted_subgraphs(i).node_names = subgraph_node_names'; 
        extracted_subgraphs(i).densities = subgraph_densities; 
        extracted_subgraphs(i).node_ids = subgraph_node_ids; 
        overall_set_of_extracted_node_ids = [overall_set_of_extracted_node_ids ; subgraph_node_ids] ; 
        
        %% do some visualization 
        gv_file_name = [figure_dir filesep sprintf('svm_clique%d', i)];  
        viewer_options.graphviz_visualize = 1; 
        viewer_options.matrix_visualize = 1; 
        graph_viewer(G, subgraph_node_ids, graph_colors, gv_file_name, viewer_options); 

        %% update graphs by removing the currently selected subgraph 
        for j=1:num_graphs 
            G{j} = Graph.RxnRxnGraph(double(G1{j}), G1{j}.node_names, G1{j}.name); 
            %%% Error checking:  check zeroing out is happening 
            % if DEBUG 
            %     A = double(G1{j}); 
            %     for l=1:(i-1)
            %         prev_nodes = extracted_subgraphs(l).node_ids; 
            %         val = A(prev_nodes, prev_nodes); 
            %         val = reshape(val , [prod(size(val)) 1]);
            %         assert(all(val == 0), 'error in zeroing\n'); 
            %     end
            % end
        end
    end


    overall_set_of_extracted_node_ids = unique(overall_set_of_extracted_node_ids);
    n_nodes = length(overall_set_of_extracted_node_ids); 

    %% Look at the overall set of extracted nodes - don't do graphviz
    fname = [figure_dir filesep 'svm_overall']; 
    viewer_options.graphviz_visualize = 0; 
    viewer_options.matrix_visualize = 1; 
    graph_viewer(G_orig, overall_set_of_extracted_node_ids, graph_colors, fname, viewer_options);
    title('Overall SVM clique extraction'); 
    %% Save results here 
    svm_result = struct('cliques', extracted_subgraphs, 'id', overall_set_of_extracted_node_ids, 'name', common_node_list(overall_set_of_extracted_node_ids)); 
    save(output_file, 'svm_result', '-append'); 
    
end



%% MKL-based processing 
if RUN_MKL_FLAG
    
    fprintf(2, 'tcga_subnetworks(): Running MKL-based analysis (does averaging of graphs)\n'); 
    extracted_subgraphs = struct('node_names', {}, 'densities', {}, 'node_ids' , {}); 
    overall_set_of_extracted_node_ids = []; 

    for i=1:num_graphs
        G{i} = G_orig{i}; 

    end



    %% Run MKL for num_cliques rounds    
    for i = 1:num_cliques
        for i=1:num_graphs
            fprintf(2, 'tcga_subnetworks(): complementing graph %d/%d\n', i, num_graphs); 
            H{i} = G{i}.complement;
        end
        %% Set MKL options 
        model_file = 'trial.out'; 
        gmkl_opts = get_gmkl_options(H, model_file); 
        gmkl_opts.svm_solver = '1'; 
        gmkl_opts.svm_solver = '5'; 
        gmkl_opts.mkl_constraint = '0';
        
        %% Run MKL 
        [gmkl_model, status, result, gmkl_cmd] = spg_gmkl(gmkl_opts); 
        alpha = gmkl_model.alpha ./ max(gmkl_model.alpha); % this is what MKL gives which is used to sort the nodes in order to extract a dense subgraph 

        %% Postprocessing - same as for SVM 
        [subgraph_node_names, G1, subgraph_densities, subgraph_node_ids]=  tcga_postprocess(G,  alpha, min_subgraph_size, max_subgraph_size);        
        k = length(subgraph_node_ids); 
        fprintf(2, 'tcga_subnetwork(): Extracted subgraph of size %d with min_density: %g\n',k, min(subgraph_densities)); 
        disp(subgraph_node_names); 
        disp(subgraph_densities);
        extracted_subgraphs(i).node_names = subgraph_node_names; 
        extracted_subgraphs(i).densities = subgraph_densities; 
        extracted_subgraphs(i).node_ids = subgraph_node_ids; 
        overall_set_of_extracted_node_ids = [overall_set_of_extracted_node_ids ; subgraph_node_ids] ; 
        
        %% do some visualization 
        gv_file_name = sprintf('clique_%d.gv', i); 
        [file_name] = graph_viewer(G, subgraph_node_ids, graph_colors, gv_file_name); 

        %% update graphs by removing the currently selected subgraph 
        for j=1:num_graphs 
            G{j} = Graph.RxnRxnGraph(double(G1{j}), G1{j}.node_names, G1{j}.name); 
            %%% Error checking:  check zeroing out is happening 
            % if DEBUG 
            %     A = double(G1{j}); 
            %     for l=1:(i-1)
            %         prev_nodes = extracted_subgraphs(l).node_ids; 
            %         val = A(prev_nodes, prev_nodes); 
            %         val = reshape(val , [prod(size(val)) 1]);
            %         assert(all(val == 0), 'error in zeroing\n'); 
            %     end
            % end
        end
    end


    overall_set_of_extracted_node_ids = unique(overall_set_of_extracted_node_ids);
    n_nodes = length(overall_set_of_extracted_node_ids); 
    
    mkl_result =  struct('cliques', extracted_subgraphs, 'id', overall_set_of_extracted_node_ids, 'name', common_node_list(overall_set_of_extracted_node_ids)); 
    save(output_file, 'mkl_result', '-append'); 

end


%% Get ordered densities 
%
% node_order - represents the order for getting first cluster
% min_subgraph_size - subgraph size to visualize
% subgraph_densities - sorted densities for the subgraph if choosing according to node order in each graph
% 

%% zero out and rerun 


% Results - we should see that the two methods 
% MKL - dense graph over complement 
% SVM - independent set over original using libsvm 
% yield similar results
