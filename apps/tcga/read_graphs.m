function [G]=read_graphs(file_name_list, dense_ids, sparse_ids)
% Read graphs and setup for dense network reconstruction 
if nargin < 3
    sparse_ids = []; 
end

num_dense = length(dense_ids); 
num_sparse = length(sparse_ids); 

fprintf(2, 'read_graphs(): reading %d dense and %d sparse graphs\n', num_dense, num_sparse); 
G = cell(num_dense + num_sparse, 1); 


is_dense = 1; 
for i=1:num_dense
    fprintf(2, 'read_graphs(): %d/%d dense subgraph\n', i, num_dense); 
    curr_file = file_name_list{dense_ids(i)};     
    G{i} = read_from_mat_file(curr_file, is_dense); 
end

is_dense = 0; 
for i=1:num_sparse
    j = num_dense + i; 
    fprintf(2, 'read_graphs(): %d/%d dense subgraph\n', i, num_sparse); 
    curr_file = file_name_list{sparse_ids(i)}; 
    G{j} = read_from_mat_file(curr_file, is_dense); 
end

function [G] = read_from_mat_file(file_name, is_dense)
% Reads from matlab file if present in [data_dir '/mat']
[pathstr, name, ext] = fileparts(file_name); 
mat_file = [pathstr '/mat/' name '.mat']; 
if exist(mat_file, 'file')
    fprintf(2, 'read_graphs(): Reading mat file: %s.mat\n', name); 
    load(mat_file); 
    if is_dense 
        G = G_dense; 
    else
        G = G_sparse; 
    end
else    
    G = Graph.readRxnRxnGraph(file_name); 
    if is_dense == 0
        G = G.complement; 
    end
end

        
    