function [H, node_list, old_idx, new_idx]=do_node_union(G)
% DO_NODE_CONVERSION convert all graphs to a common node set. 
% 
% Usage: [H]=do_node_conversion(G, conversion_mode)
% 
% conversion_mode == 0: 
%   all nodes that are not present in a graph are added as a
%   node with no edges connecting to it. 
% 
% conversion_mode == 1:
%   all nodes not present in at least one graph are removed.

num_graphs = length(G); 
fprintf(2, 'do_node_union(): converting %d graphs to union of all their nodes\n', num_graphs); 
H = cell(num_graphs, 1); 
common_node_mp = containers.Map('KeyType', 'char', 'ValueType', 'int32'); 
old_idx = cell(1, num_graphs); 
new_idx = cell(1, num_graphs); 
% add the keys to obtain a common node set - this can be made faster. 
total_node_num = 0; 
node_list = {}; 
for i=1:num_graphs
    curr_graph = G{i}; 
    curr_nodes = curr_graph.node_names; 
    curr_num_nodes = length(curr_nodes);
    c_old_idx = zeros(curr_num_nodes, 1); 
    c_new_idx = zeros(curr_num_nodes, 1); 
    x = 0; 
    y = 0; 
    for j=1:curr_num_nodes
        k = curr_nodes{j}   ;     
        x = x + 1;         
        if ~common_node_mp.isKey(k)
            y = y + 1;   
            total_node_num = total_node_num + 1;
            common_node_mp(k) = total_node_num;
            node_list{total_node_num} = k ;
        end
        c_old_idx(x) = j; 
        c_new_idx(x) = common_node_mp(k);         
    end
    old_idx{i} = c_old_idx;
    new_idx{i} = c_new_idx; 
    fprintf(2, 'do_node_union(): Added %d nodes out of %d nodes in graph %d (of %d)\n', y, x, i, num_graphs); 
end

for i=1:num_graphs
    curr_graph = G{i}; 
    old_data = double(curr_graph); 
    new_data = sparse(total_node_num, total_node_num);        
    new_data(new_idx{i}, new_idx{i}) = old_data(old_idx{i}, old_idx{i}); 
    curr_name = curr_graph.name;
    H{i} = Graph.RxnRxnGraph(new_data, node_list, curr_name); 
    fprintf(2, 'do_node_union(): converted graph %d of %d\n', i, num_graphs); 
end

