% script to get equivalence between different SVM solvers 
% clc;
% clear all;
% close all; 

n = 30; 
p = 0.5; 
k = 10;
warning('on', 'lmb:verbose'); 

%% Graph generation 
[~, A] = getPlantedClique(n, p, k); 
[K, rho] = getPsdKfromA(A, 'use_eigen', true); 

%% Using SVM - finds independent set 
model2 = findIndependentSetCSVM(K); 
alpha2 = model2.x; 

K_augment = [2*K zeros(n, 1); zeros(1, n+1)]; 
% set-up SVM instan
kernelFn = @(i, j) preK(i, j, K); 
labels = [ones(n, 1); -1]; 

K1 = [(1:(n+1))', K_augment] ; 
tic; 
model = svmtrain(labels, K1, sprintf(' -q -s 0 -c %g -t 4', n)); 
tc = toc; 
model.t = tc; 
b = zeros(n+1, 1); 
b(model.SVs) = model.sv_coef; 
model.x = b(1:n); % note the N-1 as last element is of opposite sign.  
model.v = sum(model.x); % obj(model.x, K); % the objective function 

fprintf(2, 'test_svm(): svmtrain_time: %g findIndependentSetCSVM: %g diff: %g\n', ...
        model.t, model2.t, norm(model.x - model2.x , 'fro')); 