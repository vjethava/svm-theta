n = 1000; 
p = 0.5; 
k = 3 * ceil(sqrt(n)); 
m = 10 * k; 

A = erdosRenyi(n, p) ; % getPlantedClique(n, p, k); 
[K, rho] = getPsdKfromA(A); 


%% Kernel matrix with sample id's in first row
Ka = [2*K zeros(n, 1); zeros(1, n+1)]; 
labels = [ones(n, 1); -1]; 

% nystrom approximation 
sampling_method = 1; 


tic; 
[model] = findIndependentSetCSVM(K); 
t_libsvm = toc; 

% frac =  [10:10:100];
% nf = length(frac); 
% sampling_method = 0; % uniform without replacement 

% vals = {}; 
% for i=1:nf
%     cfrac = frac(i); 
    
%     [K1, X1] = kernel_nystrom_approx(K, ceil(cfrac/100 * (n)) , 1); 
%     cx = oneClassSVM(K1); 
%     vals{i} =  cx; 
% end
% figure; 
% for i=1:nf
%     subplot(2, 1, 1) ; plot(i, norm(model.x - vals{i}, 1) , 'b.' ); hold on ; 
%     subplot(2, 1, 2); plot(i, norm(model.x - vals{i}, inf) , 'g*' ); hold on; 
% end

f=  figure; hold on; grid on; 
rv=  [10:10:100]; 
for i=1:length(rv); 
    r = rv(i); 
    m = 10 * r; 
    [K1, X1] = kernel_nystrom_approx(K, m, sampling_method, r);
    [err_bound, rank_err, diag_err] = nystrom_approx_err_bound(K, r, m); 
    true_err = norm(K - K1); 
    plot(r, true_err, 'b.'); 
    plot(r, rank_err, 'gs'); 
    plot(r, err_bound, 'rd'); 
end

[Uk, Sk, Vk ] = svd(K); 
g = figure; hold on; grid on; 
Vkt = Vk' ; 
cK = zeros(n, n); 
for i=1:n 
%     cK = Uk(:, 1:i) * Sk(1:i , 1:i) * Vkt(1:i, :);
    cK = cK + Uk(:, i) * Sk(i, i) * Vkt(i, :); 
    cErr = norm(K - cK, 'fro');
    plot(i, cErr, 'bs'); 
end
