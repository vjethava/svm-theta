%%% This script presents a simple example to check that SVM is working

clc; clear all; close all; 

n =25; 
p = 0.5; 
k = 15;
warning('off', 'lmb:verbose'); 

%% Graph generation 
[~, A2] = getPlantedClique(n, p, k); 
[K2, rho2] = getPsdKfromA(A2, 'use_eigen', true); 

%% Using SVM - finds independent set 
tic; 
model2 = findIndependentSetCSVM(K2); 
t_svm = toc ; 
alpha2 = model2.x; 

%% Using MKL - finds dense subgraph, so need to complement
G{1} = Graph(A2);
G{2} = Graph.ErdosRenyi(n, 0.4); 
% for j=3:5
%     G{j} = Graph.ErdosRenyi(n, j*0.05); 
% end
    
% G{2} = G{2}
for i=1:length(G)
    H{i} = G{i}.complement;
end


[K, rho, Y, C]=SimpleMKL.get_mkl_kernel(H);
options = SimpleMKL.get_mkl_options();
tic; 
[alpha, model] = SimpleMKL.run_mkl(K, Y, C, options, 1);
t_simplemkl = toc; 
% Results - we should see that the two methods 
% MKL - dense graph over complement 
% SVM - independent set over original using libsvm 
% yield similar results

% [node_order, s_density, alpha_sorted] = Graph.get_ordered_density(G{1}, model2.x, 1.0/n); 
% fprintf(2, 'node_order sorted_density sorted_alpha\n'); 
% [node_order s_density alpha_sorted]

model_file = 'single.out'; 

output = struct('opts', {}, 'status', [], 'result', {}, 'model', {}, 'num_kernels', [], 'time', [], 'diff', [] ); 
count= 0; 
lvec = [0.5] ;
for i=1 % solver
    for j=0:1 % constraint
        for k=1 % svm_regularizer 
            for l=1% svm_lp_regularizer
                for m=lvec
                    for s1=1
                        count = count+1; 
                        gmkl_opts.use_shrinking = sprintf('%d', s1); 
                        gmkl_opts = get_gmkl_options(G, model_file); 
                        gmkl_opts.lambda = sprintf('%g', m)        ; 
                        gmkl_opts.svm_solver = sprintf('%d', i); 
                        gmkl_opts.svm_regularizer = sprintf('%d', k); 
                        gmkl_opts.svm_lp_regularizer = sprintf('%d', l); 
                        gmkl_opts.mkl_constraint = sprintf('%d', j) ; 
                        gmkl_cmd = get_gmkl_cmd(gmkl_opts) ; 
                        tic; 
                        [status, result] = unix(gmkl_cmd) ; 
                        t_mkl = toc; 
                        
                        gmkl_model = read_gmkl_model_file(model_file); 
                        x = zeros(n, 1); 
                        x(gmkl_model.idx) = gmkl_model.x;
                        y = x ./ max(abs(x)); 
                        
                        
                        output(count).lambda = m; 
                        output(count).num_kernels = gmkl_model.num_kernels; 
                        output(count).status = status;
                        output(count).result = result; 
                        output(count).opts = [i ; j; k; l; m; s1]; 
                        output(count).model = gmkl_model; 
                        output(count).time = t_mkl; 
                        output(count).diff = [norm(y - alpha2, Inf) ; norm(y - alpha, Inf); norm(x - alpha, Inf)]; 
                        
                        fprintf(1, 'cmd status: %d\n', status); 
                        fprintf(1, 'cmd : %s\n', result); 



                        [i  j k l m] 
                        if (status == 0) &&  (gmkl_model.num_kernels == length(G)) 
                            
                            fprintf(2, 'num_kernels %d || alpha - y ||: %g t_svm: %g t_mkl: %g t_simplemkl: %g\n', ...
                                    gmkl_model.num_kernels, norm(y-alpha2, 'fro'), t_svm, t_mkl, t_simplemkl);  
                        else 
                            output(count).status = 1; 
                            fprintf(2, 'Failed optimization\n'); 
                        end
                        % [alpha2 alpha x y] 
                        
                    end 
                end
                
            end
        end
    end
end

X =  [output.status; [output.opts] ; [output.diff] ; [output.time]]; 
X(1:end, ([output.status]== 0))
