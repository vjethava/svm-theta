n = 10000; 
p = 0.5; 
k = 5*ceil(sqrt(n)); 
[~, A] = getPlantedClique(n, p, k); 
[K, rho] = getPsdKfromA(A, 'use_eigen', 1); 
tic; 
libsvm_model = findIndependentSetCSVM(K); 
t_libsvm = toc; 
x1 = libsvm_model.x ;


K1 = [K zeros(n, 1); zeros(1, n+1)]; 
labels = [ones(n, 1); -1]; 
K_pos = K1 + eye(n+1)/n^2; 
X = chol(K_pos); % K_pos = X' * X
fprintf(2, 'Error in cholesky || K_pos - X^T * X ||_Inf: %g\n' , norm(K_pos - X'*X, Inf)); 

liblinear_options = sprintf('-s 3 -c %d',  n);  % l1-loss l2-regularized SVC
model = train(labels, sparse(X), liblinear_options, 'col'); 
t_liblinear = toc;  
model.x = model.w(1:n)'; % note the N-1 as last element is of opposite sign.  
model.v = sum(model.x); % obj(model.x, K); % the objective function 
x2 = model.x; 

diff = norm(x1 - x2, Inf); 
fprintf(2, 's: %d t_svm: %g  t_linear: %g diff: %g\n', i, t_libsvm, t_liblinear, diff); 
vv = model.w; 
