function []=write_gmkl_kernel_file(K, filename)
fprintf(2, 'spg_gmkl(): writing kernel to file: %s\n', filename); 
n = size(K, 1); 
fid = fopen(filename, 'w');
fprintf(fid, '%d %d\n', n , n); 
fclose(fid); 
dlmwrite(filename, K, '-append', 'delimiter', ' '); 
% for i=1:n 
%     for j=1:n 
%         fprintf(fid, '%g', K(i, j)); 
%         if j < n
%             fprintf(fid, ' '); 
%         else
%             fprintf(fid, '\n'); 
%         end
%     end
% end

end
