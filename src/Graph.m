classdef Graph < double
  %GRAPH Generic weighted undirected graph class
  properties (Constant, Access='private')
    DIMACS_DIR=[getenv('SVM_THETA_HOME') '/data/'];
    TMP_DIR='/tmp';
  end
  %% Static methods - graph generation 
  methods (Static)     
    function [svmStruct]=csvm(K)
        % K = obj.K; 
        N = size(K, 1);
        K_augment = [2*K zeros(N, 1); zeros(1, N) 0]; 
        xx = [1:(N+1)]'; 
        yy = [ones(N, 1); -1];
        kfun = @(xi, xj) K_augment(xi, xj);
        svmStruct = svmtrain(xx, yy, 'Kernel_Function', kfun, 'method', 'SMO'); 
    end
    
    function [obj]=ErdosRenyi(N, p)
      G = erdosRenyi(N, p); 
      obj=Graph(G); 
      obj.type = 'ErdosRenyi'; 
      obj.name = sprintf('G(%d,%.4g)', N, p); 
      obj.p = p; 
    end
    
    function [node_order, subgraph_density, alpha_sorted] = get_ordered_density(G, alpha, c)
        if nargin < 3
            c = 0; 
        end
        A = double(G); 
        [alpha_sorted, node_order] = sort(alpha, 'descend');
        valid_idx = alpha_sorted > c; 
        node_order = node_order(valid_idx); 
        alpha_sorted = alpha_sorted(valid_idx); 
        ns = length(node_order); 
        subgraph_density = zeros(ns, 1);   
        curr_edges = 0; 
        for i=2:ns
            curr_node = node_order(i);
            prev_nodes = node_order(1:(i-1)); 
            edges_added = sum(A(curr_node, prev_nodes)) + sum(A(prev_nodes, curr_node)); 
            curr_edges = curr_edges + edges_added;  
            subgraph_density(i) =  curr_edges/(i*(i-1)); 
        end
    end
    
    function [Gu]=union_graph(G)
        num_graphs = length(G); 
        A = double(G{1}); 
        N = G{1}.N
        for j=2:num_graphs
            assert(G{j}.N == N, 'Graphs should be of same size'); 
            A = A + double(G{j});
        end
        Gu = Graph(sign(A)); 
        Gu.name = 'union_graph';
    end
     
    function [obj]=DIMACS(name)
      edge_file = [Graph.DIMACS_DIR name '.clq'];
      edges = load(edge_file);
      E = size(edges, 1);
      N = max(max(edges));
      A = zeros(N, N, 'double'); 
      for i=1:E   % this part is slow - see bin2asc.c and genbin.h
          A(edges(i, 1), edges(i, 2)) = 1;
      end
      A = A + A'; % symmetric edges ij \in E => ji \in E
      obj = Graph(full(A)); 
      obj.type = 'DIMACS';
      obj.name = name;      
    end
    
    function [obj]=RxnRxnGraph(A, nodeNames, name)
        obj = Graph(A); 
        obj.type='RxnRxnGraph'; 
        obj.name = name;
        obj.node_names = nodeNames;
    end
    
    function [obj]=readRxnRxnGraph(file_name)        
        fprintf(2, 'Graph.readRxnRxnGraph(): Reading graph from file: %s\n', file_name);
        try 
            fid = fopen(file_name , 'r');
            [VB, VE] = textread(file_name, '%s%s', 'delimiter','\t');
        catch  ME
            disp(sprintf('Graph.readRxnRxnGraph(): Error %s while reading %s. Did you use full paths?\n', ME.identifier, file_name));
            rethrow(ME);
        end
        T = [VB VE] ; 
        num_edges = size(T , 1);
        nv = 0; 
        num_repeated = 0; 
        A = sparse([], [], [], num_edges, num_edges, num_edges);
        nodeNameMp = containers.Map('KeyType', 'char', 'ValueType', 'int64') ;        
        nodeNames  = {}; 
        for i=1:num_edges
            curr_id = zeros(2, 1); 
            for j=1:2
                
                curr_node = T{i, j}; 
                if ~nodeNameMp.isKey(curr_node)
                    nv = nv + 1; 
                    nodeNameMp(curr_node) = nv;
                    nodeNames{nv} = curr_node;  
                end
                curr_id(j) = nodeNameMp(curr_node); 
            end            
            cval = A(curr_id(1), curr_id(2)); 
            if cval == 0 
                A(curr_id(1), curr_id(2)) =  1; 
            else
                num_repeated = num_repeated + 1; 
            end
            
        end  
        orig_nnz = nnz(A); 
        A = A(1:nv, 1:nv); 
        assert(orig_nnz == nnz(A), 'Graph.readRxnRxnGraph(): Error in node counting\n'); 
        fprintf(2, 'Graph.readRxnRxnGraph(): num_nodes: %d num_edges: %d num_repeated: %d\n', nv, nnz(A), num_repeated);
        [pathstr, name, ext] = fileparts(file_name); 

        obj = Graph.RxnRxnGraph(A, nodeNames, name);
    end
    
    % Return an instance of planted clique graph 
    function [obj]=PlantedClique(N, p, k)
      A = getPlantedClique(N, p, k); 
      obj=Graph(A); 
      obj.type = 'PlantedClique';
      obj.p = p;
      obj.k = k; 
      obj.name = sprintf('G(%d,%.4g,%d)', N, p, k); 
    end
  end
  
  %% Properties dependent on the graph 
  properties (SetAccess='immutable')
      N                % number of nodes
      E                % number of edges
      max_deg          % maximum degree
      avg_deg          % average degree
      density          % graph density 2*E/N*(N-1)
      is_directed      % whether the graph is symmetric (or directed)
  end
  
  % properties related to specific graph types
  properties (SetAccess='protected')
    type
    p                 % valid only for random graphs
    k                 % clique size
    node_names        % names of the nodes
  end
  
  %% SVM properties
  properties
    name
    K 
    rho 
    model
  end
  
  %% Generic functions related to SVM
  methods      
%     function [H] = subsref(obj, S)
%         H = subsref(double(obj), S) ;
%     end
%     
%     function obj = subsasgn(obj, S, B)
%         obj = subsasgn@double(obj,S, B); 
%     end    
    
%     % Check whether given graph is symmetric
%     function [symmetric] = get.is_symmetric(obj)
%         disp(obj.is_symmetric);
%         symmetric =@my_nested; 
%             function [z] = my_nested()
%                 fprintf(2, 'calling check\n'); 
%                
%             end
%     end  
%     
    function [k, r] = compute_psd(obj)    
        v = eig(double(obj)); 
        r = -v(1);
        k = eye(obj.N, obj.N) + double(obj)/r; 
    end
    
    function [k]=get.K(obj)
      k = eye(obj.N, obj.N) + double(obj)/obj.rho; 
    end
    
    function disp_rho(obj)
        fprintf(2, 'rho: %g\n', obj.rho); 
    end
    
    function [G]=complement(obj)
      n = obj.N; 
      a_c = ones(n, n, 'double') - eye(n, n, 'double') - double(obj); 
      G = Graph(a_c); 
      if ~isempty(obj.name) 
          if regexp(obj.name, '_complement$')
              G.name =  regexprep(obj.name, '_complement$', ''); 
          else              
              G.name = [obj.name '_complement'];
          end
      end
    end
    
    function obj=set.K(obj, k)
      obj.K = k;
    end
    
    function obj=set.rho(obj, r)
      obj.rho = r; 
    end
    
    function disp(obj)
      if ~isempty(obj.name)
        fprintf(1, '\tname:\t%s\n', obj.name);
      end
      fprintf(1, '\tG.N:\t%d\n', obj.N); 
      fprintf(1, '\tG.E:\t%d\n', obj.E); 
    end
  end
  
  
  methods      
      function [delta] = get_avg_degree(obj)
          if obj.N > 0
              delta = (2*obj.E)/(obj.N);
          else
              delta = 0;
          end
      end
      
      function [Delta] = get_max_degree(obj)
          Delta = max(sum(obj));
      end
      
      function [z] = check_directed(obj)
          z = true;
          tl = tril(obj, 0);
          th = triu(obj, 0);
          if tl' == th
              z = false;
          end
      end
  end
  
  methods 
    
  end
  
  %% Dependent property methods
  methods
    
    function obj=Graph(data)
      % Constructor - sets all immutable properties
      obj=obj@double(data);
      obj.is_directed = obj.check_directed(); 
      obj.N = size(data, 1); 
      obj.E = nnz(data);
      obj.avg_deg = obj.E/obj.N;
      obj.max_deg = max(sum(data));
      obj.density = obj.E / (obj.N * (obj.N - 1)); 
    end   
    
    
%     function [n]=get.N(obj)
%       n = size(obj, 1);
%     end
%     
%     function [e]=get.E(obj)
%       e = nnz(triu(obj));       
%     end
%         
%     function [density] = get.density(obj)
%         if density < 0
%             density = 2*obj.E/(obj.N*(obj.N-1));       
%         end        
%     end
%     
%  
% %     function [B]=subsref(obj, S)
% %       fprintf(2, '%s\n', S.type); 
% %       keyboard; 
% %       disp(S{:}.subs); 
% %     end
%     
%     function [Delta]=get.max_deg(obj)
%       Delta = max(sum(obj)); 
%     end
%     
%     function [delta]=get.avg_deg(obj)
%       if obj.N > 0
%         delta = (2*obj.E)/(obj.N);
%       else 
%         delta = 0; 
%       end
%     end
    
  end  
end

