function [A] = read_dimacs(filename) 
try
    fid = fopen(filename, 'r'); 
    fprintf(2, 'read_dimacs(): reading file: %s\n', filename); 
catch
    error(sprintf('Could not read file: %s\n', filename)); 
end
[fileDir, fileName, fileExt] = fileparts(filename); 
fileName = [fileName fileExt];  
n = 0; 
m = 0; 
A = []; 
header_lines = true; 
while header_lines; 
    cline = fgetl(fid); 
    cvals = strsplit(cline); 
    if cvals{1} == 'c' 
        fprintf(2, 'read_dimacs(%s) comment: %s\n' , fileName , cline); 
    elseif cvals{1} == 'p'
        n = sscanf(cvals{3}, '%d'); 
        m = sscanf(cvals{4}, '%d'); 
        fprintf(2, 'read_dimacs(%s) n: %d m: %d\n', fileName , n, m); 
        header_lines = false; 
    end
end

C = textscan(fid, '%c%d%d'); 
assert(length(C{2}) == m); % have read the m edges correctly 
A = sparse(double(C{2}), double(C{3}), ones(m, 1), n, n);  
if issymmetric(A)
    as = 'symmetric';
else
    as = 'unsymmetric!';
end

fprintf(2, 'read_dimacs(%s): Finished reading %s graph n: %d m: %d.\n', fileName, as, n, m); 
fclose(fid); 