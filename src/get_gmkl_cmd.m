function [gmkl_cmd_string] = get_gmkl_cmd(options)
% GET_GMKL_CMD returns the gmkl command string depending on specified options 
%     
% Usage: [gmkl_cmd_string] = get_gmkl_cmd(options)
%
% Expects: 
% --------
% options - structure containing gmkl options generated using get_gmkl_options() 
%
% Returns:
% --------
% gmkl_cmd_string - string which can be executed using unix() command to run GMKL 
     
% Also see:
% ---------
% get_gmkl_options()
%

%% Code begins here 
gmkl_cmd_string = [ options.gmkl_path '/' options.gmkl_cmd ];
gmkl_cmd_string = [ gmkl_cmd_string ' -s ' options.svm_type]; 
gmkl_cmd_string = [ gmkl_cmd_string ' -g ' options.svm_regularizer];
gmkl_cmd_string = [ gmkl_cmd_string ' -a ' options.num_kernels]; 
gmkl_cmd_string = [ gmkl_cmd_string ' -o ' options.svm_lp_regularizer]; 
gmkl_cmd_string = [ gmkl_cmd_string ' -f ' options.svm_solver]; 
gmkl_cmd_string = [ gmkl_cmd_string ' -c ' options.svm_cost]; 
gmkl_cmd_string = [ gmkl_cmd_string ' -m ' options.cache_size]; 
gmkl_cmd_string = [ gmkl_cmd_string ' -j ' options.mkl_constraint]; 
gmkl_cmd_string = [ gmkl_cmd_string ' -h ' options.use_shrinking]; 
gmkl_cmd_string = [ gmkl_cmd_string ' -l ' options.lambda]; 
%% specify the files
gmkl_cmd_string = [ gmkl_cmd_string ' -k ' options.kernel_file] ; 
gmkl_cmd_string = [gmkl_cmd_string ' ' options.label_file]; 
gmkl_cmd_string = [gmkl_cmd_string ' ' options.model_file]; 
%% output the final command
% fprintf(2, gmkl_cmd_string); 
% fprintf(2, '\n');
end
