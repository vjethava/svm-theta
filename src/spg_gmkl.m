function [gmkl_model, status, result, gmkl_cmd, t_mkl]=spg_gmkl(gmkl_options)
% SPG_GMKL Function that uses SPG_GMKL to solve multiple kernel
% learning problem for identifying common dense subgraph. 
% 
% Usage: [model]=spg_mkl(G, options)
% 
% Expects:
% --------
% G - structure containing the different graphs 
%
% Returns: 
% --------
% model - MKL model containing results of the SVM 

    
n = gmkl_options.graph_size;     
[gmkl_cmd] = get_gmkl_cmd(gmkl_options); 
tic; 
[status, result] = unix(gmkl_cmd, '-echo'); 
t_mkl = toc; 

gmkl_model = read_gmkl_model_file(gmkl_options.model_file); 
x = zeros(n, 1); 
x(gmkl_model.idx) = gmkl_model.x;
y = x ./ max(abs(x)); 

gmkl_model.alpha = x; 


