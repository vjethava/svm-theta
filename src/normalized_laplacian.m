function [L] = normalized_laplacian(A)
% NORMALIZED_LAPLACIAN Returns L = I - D^{-0.5} A D^{-0.5}
n = size(A, 1);
D1 =  diag ( 1./ sqrt(sum(A, 1)) ); 
L = eye(n) - D1 * A * D1; 


