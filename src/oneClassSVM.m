function [x] = oneClassSVM(K)
n = size(K, 1);
cvx_begin
variable x(n, 1) nonnegative ; 
e=ones(n, 1); 
minimize quad_form(x, K) - 2*sum(x); 
subject to 
max(x) <= 1; 
cvx_end
