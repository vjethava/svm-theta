function [K1, X1, Kr] = kernel_nystrom_approx(K, m, sampling_method, r)
% KERNEL_NYSTROM_APPROX Returns approximate kernel by sampling m columns from matrix K (n x n)
% and returns the approximation: 
%    
% Usage: kernel_nystrom_approx(K, m, sampling_method, r)    
%
% Expects: 
% -------- 
% K - original kernel matrix (n x n) 
% m - number of columns to sample 
% sampling_method - how to sample
%          '0' - sample without replacement [Williams & Seeger '01]     
%          '1' - based on column norm p(i) = norm(K(:, c)) / norm(K, 'fro') [default] 
%          '2' - uniform with replacement p(i) = 1/n
%          '3' - diagonal element p(i) = K(i,i)^2 / sum(diag(K).^2) 
%    
% r - construct Kr as pinv of best rank 'r' approximation to K(c, c) [default: r=c]
%   
% Returns: 
% --------
% K1 Approximate kernel matrix (n x n) 
% X1 Approximate feature representation (n x c)
%
% Notes:
% ------
%   Kr = pinv( K(C, C) )
%   [Vr, Dr] = eig(Kr)     
%     
%   K1 = K(:, C) * Kr * K( C , :)       
%   X1 = 
%  
% See: 
% ----
% On the Nystrom Method for Approximating a Gram Matrix for Improved Kernel-Based Learning
% Drineas & Mahoney, JMLR, 2005 
% 
% Nystrom Method vs Random Fourier Features: A Theoretical and Empirical Comparison
% Yang etal NIPS, 2012
% 
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    if nargin < 4
        r = m; 
    end
    
    if nargin < 3 
        sampling_method = 1; 
    end
    
    % implement other sampling methods 
    n = size(K, 1); 
    
    if sampling_method == 0 % uniform without replacement 
        p = 1/n * ones(1, n); 
    elseif sampling_method == 1 % column norm 
        p = sum( K.* K , 1); 
        p = p ./ sum(p); 
    else
        error('other sampling methods not implemented') ;
    end
    
    C = datasample([1:n], m,  'Replace', false, 'Weights', p); 
    Khat = K(C, C);         
    

    if r < m
        [U, S, V] = svds(Khat, r); 
        Khat = U * S * V'; 
    end
    
    Kr = pinv(Khat);     
    %     nr = rank(Kr); 
    
    
    [Vr, Dr] = eig(Kr); 
    Vr = real(Vr) ; Dr = real(Dr);
    
    Kc = K(:, C); 
    
    K1 = Kc * Kr * Kc'; 
    
    DrSqrt = diag(sqrt(diag(Dr)));
    
    X1 = Kc * Vr * DrSqrt; 
    
    epsilon = 1/(n.^2); 
    fprintf(2, 'kernel_nystrom_approx(n=%d, m=%d): K - K1 Frob: %g err>%g: %g\n', n, m, norm(K - K1, 'fro'), epsilon, nnz(abs(K - K1)> epsilon)); 
    
