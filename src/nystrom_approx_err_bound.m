function [err_bound, rank_approx_err, epsilon_diag_term] = nystrom_approx_err_bound(K, r, m, my_norm, my_epsilon) 
% NYSTROM_APPROX_ERR_BOUND returns bound on error between K and its approximation factorization given by 
%  
% || K  - K_approx ||_{my_norm} <= || K - K_r ||_{my_norm} + my_epsilon \sum_i (K_ii)^2
% 
% K - original kernel matrix (n x n)
% r - rank of approximation 
% m - number of columns sampled should be O( r / my_epsilon^4 )
% my_epsilon - error term 

[Ug, Sg, Vg] = svds(K, r); 
if nargin  < 4 
    my_norm = 2; % 'fro' 
end

if nargin < 5
    my_epsilon = 1e-1; 
end

rank_approx_err = sum(diag(K).^2 ) ; 
epsilon_diag_term = norm(K - Ug * Sg * Vg' , my_norm) ; 

err_bound=rank_approx_err + epsilon_diag_term; 