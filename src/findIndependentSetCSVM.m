function [model] = findIndependentSetCSVM(K, varargin)
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
% FINDINDEPENDENTSETCSVM  Runs the C-SVM formulation to obtain the 
% independent set solving the problem 
%       \max_{x >= 0} 2e'x-x'Kx
%
% Usage: [model] = findIndependentSetCSVM(K, varargin)                   
%                                                                        
% Expects:                                                               
% ----------------------------------------                               
% K               : Kernel matrix                                                      
% 
% Returns:                                                               
% ----------------------------------------                               
% model           : Model file containing SVM results and post-processing      
%                                                                        
% Note:                                                                  
% ----------------------------------------                               
%                                                                        
% See Also:                                                              
% ----------------------------------------                               
%                                                                        
% Copyright (c) 2011, Vinay Jethava (vjethava@gmail.com)                 
%                                                                        
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 

%% Pre-processsing

obj = @(x, K) 2*sum(x) - x'*K *x;

%% Initialization 
assert(size(K, 1) == size(K, 2)); 
n = size(K, 1); 
[C, varargin] = vj_process_options(varargin, 'C', n); 

%% Kernel matrix with sample id's in first row
K1 = [(1:n)' 2*K zeros(n, 1); (n+1) zeros(1, n+1)]; 
labels = [ones(n, 1); -1]; 

%% Run the SVM
tic; 
model = []; 
if exist('libsvmtrain', 'file') 
    warning('lmb:verbose', 'findIndependentSetCSVM() using libsvmtrain'); 
    model = libsvmtrain(labels, K1, sprintf('-s 0 -c %g -t 4', n)); 
else % hope for the best
    warning('lmb:verbose', 'findIndependentSetCSVM() using svmtrain'); 
    model = svmtrain(labels, K1, sprintf('-s 0 -c %g -t 4', n)); 
end

tc = toc; 

%% Update the model 
model.t = tc; 
b = zeros(n+1, 1); 
b(model.SVs) = model.sv_coef; 
model.x = b(1:n); % note the N-1 as last element is of opposite sign.  
model.v = sum(model.x); % obj(model.x, K); % the objective function 
