function [model]=read_gmkl_model_file(filename)
fid = fopen(filename, 'r'); 
model = struct; 
model = read_model_attribute(fid, model); % read svm_type
model = read_model_attribute(fid, model, 'num'); % read nr_class
model = read_model_attribute(fid, model, 'num'); % read num_kernels
kernels = struct('w', [], 'u', [], 'file', {}); 
for i=1:model.num_kernels
    curr_line = fgets(fid); 
    a = strsplit(curr_line); 
    kernels(i).w = str2double(a{4}); 
    kernels(i).u = str2double(a{6});
    kernels(i).file = a{8};     
end
model.kernels = kernels; 
model = read_model_attribute(fid, model, 'num');      % total_sv
model = read_model_attribute(fid, model, 'double');   % rho
curr_line = fgets(fid); % discard labels

model = read_model_attribute(fid, model, 'num');      % nr_SV
fgets(fid); % SV - begins here 
model.x = zeros(model.nr_sv, 1); 
model.idx = zeros(model.nr_sv, 1); 
for i=1:(model.nr_sv)
    curr_line = fgets(fid);
    a = strsplit(curr_line); 
    b = strsplit(a{2} , ':'); 
    model.x(i) = str2double(a{1}); 
    model.idx(i) = str2num(b{2}) + 1; 
end
fclose(fid); 

function [model]=read_model_attribute(fid, model, type) 
curr_line = fgets(fid); 
fprintf(2, 'spg_gmkl(): read model file line: %s\n', curr_line); 
a = strsplit(curr_line);

if nargin < 3
    model.(a{1}) = a{2};
elseif strcmp(type, 'num')
    model.(a{1}) = str2num(a{2});
elseif strcmp(type, 'double')
    model.(a{1}) = str2double(a{2});
end

