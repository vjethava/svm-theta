function [K, rho, ta] = getPsdKfromA(A, varargin)
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
% GETPSDKFROMA Compute K = I + A/(-\lambda_\min(A)) s.t. K is positive 
% semi-definite                                                           
%                                                                        
% Usage: [K] = getPsdKfromA(A)                                           
%                                                                        
% Expects:                                                               
% ----------------------------------------                               
% A               :  Matrix                                                    
%                                                                        
% Returns:                                                               
% ----------------------------------------                               
% K               :  Positive semi-definite matrix                                                    
%                                                                        
% Note:                                                                  
% ----------------------------------------                               
%                                                                        
% See Also:                                                              
% ----------------------------------------                               
%                                                                        
% Copyright (c) 2011, Vinay Jethava (vjethava@gmail.com)                 
%                                                                        
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
[maxN, varargin] = vj_process_options(varargin, 'maxN', 10000); 
[use_max_deg, varargin] = vj_process_options(varargin, 'use_max_deg', ...
                                          0);  
[use_eigen, varargin] = vj_process_options(varargin, 'use_eigen', 0);  
[use_lanczos, varargin] = vj_process_options(varargin, 'use_lanczos', 1); 
[verbose, varargin] = vj_process_options(varargin, 'verbose', 0); 
assert(use_eigen * use_max_deg == 0, 'Cannot do both max_deg and eigen'); 
getMaxDeg = @(A) max(sum(A, 1)); 
N = size(A, 1); N = size(A, 1); 

tic; 
if ((N > maxN)  || (use_max_deg == 1)) && (use_eigen == 0)
    rho = getMaxDeg(A);
    method='max_deg';
else
    method='eigen';
    if use_lanczos && exist('laneig') % have propack installed and working
        warning('lmb:verbose', 'getPsdKfromA(): using laneig()\n'); 
        try 
            lambdaMinOfA = laneig(A, 1, 'AS'); 
        catch ME % For some reason laneig did not work - fall back to eigs() 
            warning('lmb:verbose', 'getPsdKfromA(): laneig() failed to run - reverting to eigs() \n'); 
            lambdaMinOfA = eigs(A, 1, 'SA'); 
        end
    else
        lambdaMinOfA = eigs(A, 1, 'SA'); 
    end
    rho = -  lambdaMinOfA ;
end
K  = eye(N) + A/(1.00001 * rho);  % make it positive definite! not just positive semi-definite 
ta = toc; 
warning('lmb:verbose', 'getPsdKfromA() Used: %s Time taken: %g\n', method, ta); 