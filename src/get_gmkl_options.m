function [options]=get_gmkl_options(G, model_file, varargin)
% GET_GMKL_OPTIONS 
%  
% [options]=get_gmkl_options(G, model_file)
% 
% Expects: 
% --------
% G - cell containing Graph data structures
% model_file - file name for saving the model 
%
% Returns: 
% --------
% options - structure containing options for GMKL 
%
% Also see: 
% ---------
% gmkl_cmd_string = get_gmkl_cmd(options)
%
%%
fprintf(2, 'get_gmkl_options(): generating spg_gmkl options\n'); 
m = length(G);      % number of graphs
n = G{1}.N;         % number of nodes in each graph



[kernel_file, varargin] = process_options(varargin, 'kernel_file', [tempdir 'kernel_list'] ); 
[label_file, varargin] = process_options(varargin, 'label_file', [tempdir 'y_train']); 

%% Construct the options 
options = struct(); 
options.gmkl_path= [getenv('SVM_THETA_HOME') '/lib/spg-gmkl']; 
options.gmkl_cmd = 'svm-train'; 

options.graph_size = n; 
options.num_kernels = sprintf('%d' , m); 
options.kernel_file =kernel_file; 
options.label_file = label_file; 
options.svm_type = '0'; 
options.svm_cost = sprintf('%d', n);
options.svm_regularizer = '1'; % 0 -Entropic, 1 - L1, 2 - L2, 3 Lp
options.svm_lp_regularizer = '1'; 
options.svm_solver = '2'; % 1 - SMO, 2 - SimpleMKL, 3 - VSKL 
options.mkl_constraint = '1'; % 0 - simplex, 1 - non-negative orthant
options.cache_size = '1000'; % cache size in MB
options.lambda = '0.5'; 
options.use_shrinking = '0'; % 0 - off, 1 - on 
%% write the kernels 
fid  = fopen(options.kernel_file, 'w'); 
fprintf(2, 'get_mkl_options(): writing list of kernels to file: %s\n', options.kernel_file) ; 
for i=1:m
    curr_K = get_mkl_kernel(G{i}); 
    curr_kernel_fname = [tempdir sprintf('/kernel_%d', i)]; 
    write_gmkl_kernel_file(curr_K , curr_kernel_fname); 
    fprintf(fid, '-t 4 -f %s\n', curr_kernel_fname); 
end
fclose(fid); 
%% write the labels
fid = fopen(options.label_file, 'w'); 
fprintf(2, 'get_gmkl_options(): writing labels to file: %s\n', options.label_file);  
for i=1:n 
    fprintf(fid, '1\n'); 
end
fprintf(fid, '-1\n');
fclose(fid); 
options.model_file = model_file ;
end


function [K] = get_mkl_kernel(G)
fprintf(2, 'spg_gmkl(): computing mkl kernel for %s\n', G.name);  
A_G = double(G); 
N = size(A_G, 1); 
[k, rho] = getPsdKfromA(full(A_G), 'use_max_deg', 1); 
K = [2*k zeros(N, 1); zeros(1, N+1)];
end

 
