% Compile the PROPACK package
% Copyright LI, Wei(@kuantkid)

% detect gfortran
% switch (lower(computer))
%   case 'glnxa64'
%     fc = 'gfortran';
%     suffix = 'mexa64';
%   case 'glnxa32'
%     fc = 'gfortran';
%     suffix ='mexa32';
%   otherwise
%     error('Work Only on Linux');
%     % ('Please define the location of gfortran/fortran77 location');
% end    

fc = 'gfortran'
suffix = 'mexmaci64'
libsubdir = 'maci64' 

% clear the package
% system(['rm *.' suffix]);

display([ repmat(' ',[1 10]) 'Compiling Fortran Subroutines']);
system([fc ' -c -lblas -fPIC -o tqlb.o tqlb.f']);
system([fc ' -c -lblas -fPIC -o dbdqr.o dbdqr.f']);
system([fc ' -c -lblas -fPIC -o reorth.o reorth.f']);
display('Done');

% lapack
% lapack = [matlabroot '/bin/glnxa64/libmwlapack.so'];
% lapack = [matlabroot '/bin/glnxa64/mkl.so']; % mkl
                                             % blas = [matlabroot '/bin/glnxa64/libmwblas.so'];
                                             % blas   = [matlabroot '/bin/glnxa64/mkl.so']; % mkl

libsubdir = 'maci64' 
blas= [matlabroot '/bin/' libsubdir '/libmwblas.dylib']
lapack = [matlabroot '/bin/' libsubdir '/libmwlapack.dylib'];
libmkl = [matlabroot '/bin/' libsubdir '/mkl.dylib'];

% eval(['mex reorth_mex.c reorth.o ' blas ]);
eval('mex tqlb_mex.c tqlb.o');

mex -v bdsqr_mex.c dbdqr.c -output bdsqr -llapack -largeArrayDims -lblas

% rename the mex files
system(['mv bdsqr_mex.' suffix ' bdsqr.' suffix]);
system(['mv reorth_mex.' suffix ' reorth.' suffix]);
system(['mv tqlb_mex.' suffix ' tqlb.' suffix]);