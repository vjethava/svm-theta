% save current working directory
CURR_DIR = pwd;
% check if the path is already set or not.
SVM_THETA_HOME = getenv('SVM_THETA_HOME');
if length(SVM_THETA_HOME) == 0
    SVM_THETA_HOME=pwd;
    setenv('SVM_THETA_HOME', SVM_THETA_HOME);
end

% set machine specific options in startup.m
if exist('~/startup.m')
    run ~/startup.m
end

%% Compiling libsvm
cd([SVM_THETA_HOME '/lib/libsvm-3.1']);

[status, result] = unix('make') ;
a = 'make: Nothing to be done';
if strncmp(result, a, length(a))
    disp('libsvm ready');
elseif status == 0
    disp('compiled libsvm successfully, compiling matlab part');
    cd([LIBSVM_ROOT '/matlab']);
    make;
else
    error('Could not compile libsvm');
end
setenv('LIBSVM_ROOT', [SVM_THETA_HOME '/lib/libsvm-3.1']);
cd(SVM_THETA_HOME);


%% Compiling gmkl
GMKL_ROOT=[SVM_THETA_HOME, '/lib/spg-gmkl'];
cd(GMKL_ROOT);
[status, result] = unix('make');
if strncmp(result, a, length(a))
    disp('gmkl ready');
elseif status == 0
    disp('compiled gmkl successfully');
else
    error('Could not compile gmkl');
end
cd(SVM_THETA_HOME);


%% Compiling liblinear
LIBLINEAR_ROOT = [SVM_THETA_HOME '/lib/liblinear-1.96'];
cd(LIBLINEAR_ROOT);
[status, result] = unix('make');
if strncmp(result, a, length(a))
    disp('liblinear ready');
elseif status == 0
    disp('compiled liblinear successfully, building matlab command');
    cd([LIBLINEAR_ROOT '/matlab']);
    make;
else
    error('Could not compile liblinear');
end
cd(SVM_THETA_HOME);

% %% Compiling vjlibsvmapi
% setenv('MATLAB_HOME', matlabroot);

% disp('Compiling vjlibsvmapi');
% cd([SVM_THETA_HOME '/src']);
% setenv('MATLAB_HOME', matlabroot);
% [status, result] = system('make');
% if status == 0
%     disp('compiled vjlibsvmapi successfully');
% else
%     error('Could not compile vjlibsvmapi');
% end
% cd(SVM_THETA_HOME);

%% Add the paths
disp('Adding the paths');
addpath(genpath([SVM_THETA_HOME '/lib']));
addpath( [SVM_THETA_HOME '/src'] );
addpath( [SVM_THETA_HOME '/test'] );
warning('off', 'lmb:verbose');

addpath( [ SVM_THETA_HOME, '/apps/tcga']);

% support for laneig() - Lanczos method for eigenvalue computation
addpath( [SVM_THETA_HOME '/PROPACK']);

% support for first-order methods via TFOCS
addpath( [SVM_THETA_HOME '/TFOCS-1.3.1']);
cd(CURR_DIR); % go back to original directory
clear CURR_DIR;
